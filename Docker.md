# Docker

## O que é?

Docker é uma plataforma aberta , criada com o objetivo de facilitar o desenvolvimento, a implantação e a execução de aplicações em ambientes isolados. Foi desenhada especialmente para disponibilizar uma aplicação da forma mais rápida possível. 

Docker é uma plataforma para desenvolvedores e administradores de sistemas construírem, executarem e compartilharem aplicações com contêineres. O uso de contêineres para o deploy de aplicações é chamado de conteinerização. 

Usando o Docker, você pode facilmente gerenciar a infraestrutura da aplicação, isso agilizará o processo de criação, manutenção e modificação do seu serviço. 

Todo processo é realizado sem necessidade de qualquer acesso privilegiado à infraestrutura corporativa. Assim, a equipe responsável pela aplicação pode participar da especificação do
ambiente junto com a equipe responsável pelos servidor. 

O Docker utiliza o modelo de contêiner para “empacotar” a aplicação que, após ser transformada em imagem Docker, pode ser reproduzida em plataforma de qualquer porte; ou seja, caso a aplicação funcione sem falhas em seu notebook, funcionará também no servidor ou no mainframe. Construa uma vez, execute onde quiser.

Os contêineres são isolados a nível de disco, memória, processamento e rede. Essa separação permite grande flexibilidade, onde ambientes distintos podem coexistir no mesmo host, sem causar qualquer problema. 

Essa característica de isolamento permite ter para cada aplicação um ambiente adequado para ela, assim, poderíamos ter, por exemplo, uma aplicação executando sobre NodeJS 12x, enquanto uma outra sobre NodeJS 8x, e ambas coexistindo no mesmo host.

De forma resumidamente prática, o docker é a estrutura básica que permite a criação e a execução de contêineres. 

## Contêineres

Um contêiner contém toda infra-estrutura básica para execução de uma determinada aplicação.

De forma bem simplista, para uma aplicação PHP, um contêiner teria, por exemplo, o interpretador PHP em versão determinada, bem como as extensões PHP necessárias à aplicação, e o composer. Seguindo o mesmo cenário, um outro contêiner poderia disponibilizar/expor uma base de dados relacional.

Um contêiner é baseado em uma imagem. Fazendo uma analogia com orientação a objetos, a imagem seria a classe, enquanto um contêiner seria uma instância dessa classe.

A existência dos contêineres é possível a partir de **virtualização a nível de SO**. Nesse nível de virtualização, o *kernel* do sistema operacional do Docker host permite a existência de várias instâncias de usuário, em vez de apenas uma. Do ponto de vista do usuário e dos programas que este executa, a sensação é de que os mesmos estão sozinhos na máquina e ela é um computador real e inteiro, ao invés de uma mera partição que divide recursos de hardware com outras instâncias.

Os contêineres utilizam o *host* do Docker para tarefas como conexão a redes e conexão ao *kernel*(para ter acesso ao *hardware* da máquina). Além disso, um desenvolvedor pode salvar essas configurações em [docker files](https://www.gta.ufrj.br/ensino/eel879/trabalhos_v1_2017_2/docker/faq.html#docker_file), permitindo assim que vários contêineres com configurações idênticas sejam criados, além de poder compartilhar tal arquivo com uma equipe de desenvolvimento, para que todos possam produzir software utilizando um ambiente com a mesma configuração.

Um contêiner não é uma maquina virtual. Um contêiner é executado em cima de um arquivo de SO, porém ao contrário de uma máquina virtual inteira, vários *contêineres* podem funcionar em cima de um mesmo arquivo de imagem.

![ ](https://www.gta.ufrj.br/ensino/eel879/trabalhos_v1_2017_2/docker/images/docker-vm-container.png)

### Comandos básicos

Para levantar um contêiner a partir de uma imagem:

```shell
docker container run <parâmetros> <imagem> <CMD> <argumentos>
```

Principais parâmetros

- `-d`: execução do contêiner em segundo plano

- `-i`: modo iterativo. Mantem o STDIN aberto mesmo sem console anexado

- `-t`: aloca um pseudo TTY

- `--rm`: remove o contêiner após a finalização
  
  - Não funciona com -d

- `--name`: nomeia o contêiner

- `-v`: faz mapeamento de volume

- `-p`: faz mapeamento de porta

- `-m`: limita o uso de memória RAM

- `-c`: balanceia  o uso de CPU

#### Nomeando contêiner

```shell
docker container run -it --rm --name meu_phyton phyton bash
```

O comando a acima incia  um contêiner chamado `meu_phyton`, a partir da  imagem `phyton`, que será automaticamente excluído ao finalizar, e que, ao iniciar exibirá um terminal *bash*. Comparando com o comando genérico de criação de um contêiner temos que:  `-it --rm --name meu_phyton` são os parâmetros, `phyton` é a imagem, e `bash` é o CMD, que neste caso não possui argumentos.

Obs: Caso não seja informado o CMD é executado o padrão da imagem, que é definido em seu Dockerfile. No caso do phyton é o seu terminal interativo (shell).

#### Mapeamento de porta

O mapeamento de portas segue o seguinte padrão:

```shell
-p "<host>:<container>" 
```

Logo, comando 

```shell
docker container run -it --rm -p 80:8080 python
```

inicia um contêiner a partir de uma imagem phyton, que repassará tudo que chegar na porta 80 do Docker host para porta 8080 do contêiner. A porta 8080 do contêiner não é acessível via Docker host, pois é isolada da rede.

#### Limitando o uso de memória RAM

```shell
docker container run -it --rm -m 512M python
```

Limita ao contêiner o uso de apenas 512 MB de memória

#### Balanceando o uso de CPU

O balanceamento de uso da CPU pelos contêineres é feito por meio da atribuição de pesos, que podem variar de 1 a 1024, sendo o último o valor padrão. Quanto menor o peso, menor a prioridade de execução. 

```shell
docker container run -it --rm -c 512 python
```

### Gerenciamento de contêineres

Para visualizar a listagem de contêineres de um Docker host:

```shell
docker container ls <parâmetros>
```

Principais parâmetros:

- `-a`: Lista todos os contêineres, incluindo os desligados

- `-l`: Lista os últimos contêineres, incluindo os desligados

- `-n`: Lista os últimos N contêineres, incluindo os desligados

- `-q`: Lista apenas os IDs dos contêineres

Para desligar um contêiner:

```shell
docker container stop <ID> | <nome>
```

Para desligar todos os contêineres atualmente ligados:

```shell
docker container stop $(docker container ls -q)
```

Para religar um contêiner que foi desligado  e não iniciar um novo: 

```shell
docker container start <ID> | <nome>
```

Para remover um contêiner desligado:

```shell
docker container rm <ID>
```

Para acessar um contêiner executando em *background*:

```shell
docker attach <ID> | <nome>
```

## Imagens

É a base para a criação de contêineres. Todo contêiner é iniciado a partir de uma imagem, dessa forma podemos concluir que nunca teremos uma imagem em execução.

Um contêiner só pode ser iniciado a partir de uma única imagem. Caso deseje um comportamento diferente, será necessário customizar a imagem.

### Imagens oficiais e não oficiais

Imagens oficiais, são mantidas pela empresa Docker, e não possuem usuário em seu nome: `ubuntu:16:04`. Imagens não oficiais são mantidas pelos seus criadores, e tem a seguinte nomenclatura: `nomeUsuario/ubuntu`. 

### Estrutura do nome das imagens oficiais

O nome de uma imagem oficial é composto de duas partes: repositório e *tag*. Tomando como exemplo `ubuntu:16.04`, `ubuntu` é o repositório, enquanto `16.04` é a *tag*. 

Para o docker, repositório significa uma coleção de imagens, não o local em que estão armazenadas. Logo, o repositório `ubuntu` é a coleção de todas as imagens do Ubuntu. Já a *tag*, é usada para identificar unicamente uma imagem da coleção. Cada conjunto `repositório:tag` representa uma imagem diferente.

### Comandos básicos

Lista todas as imagens baixadas no computador (Docker host):

```shell
  docker image list 
```

Para atualizar uma imagem:

```shell
docker image pull <nome_da_imagem> 
```

Para excluir uma imagem:

```shell
docker imagem rm  <id>
```

### Criando imagens customizadas com Dockerfile

O entendimento é simples: a partir de uma imagem base, informando as modificações é possível criar uma imagem Docker para atender a uma necessidade específica. `Dockerfile`  (com "D" e sem extensão) é o arquivo em que ficam listadas as modificações necessárias para a customização da nova imagem.

É possível customizar imagens sem a utilização do Dockerfile. No entanto, a grande vantagem do Dockerfile é a rastreabilidade: a possibilidade de saber, a qualquer tempo, quais foram as alterações necessárias para customização da imagem. É possível também alterar o Dockerfile e evoluir a imagem.

A seguir, temos um exemplo do conteúdo base de um arquivo Dockerfile:

```dockerfile
FROM ubuntu:16.04
RUN apt update && apt install nginx -y
COPY arquivo_teste /tmp/arquivo_teste
CMD bash
```

Onde:

- `FROM` é usado para indicar qual é a imagem base

- `RUN` é usado para informar quais comandos devem ser executados para efetuar as mudanças necessárias na infraestrutura do sistema. 
  
  - Podem existir várias linhas com o comando RUN

- `COPY` é usado para copiar arquivos da estação de trabalho para dentro da imagem

- `CMD` é usado para informar qual comando será executado, por padrão, caso nenhum seja informado na inicialização de um contêiner a partir desta imagem.

Para criar a imagem usando as definições do Dockerfile: 

```shell
docker image build -t ubuntu:nginx .
```

Em que o `-t` é utilizado para informar o nome da nova imagem e o `.` após o nome indica que o diretório atual (diretório onde se encontra o Dockerfile) será utilizado como contexto de criação da imagem (para, por exemplo, uso do comando COPY).

O Dockerfile é uma sequência de instruções executadas, uma por vez, de cima para baixo. Por isso se uma instrução depender de outra, essa deve ser descrita mais acima no documento. 

O resultado de cada instrução do Dockerfile é armazenado em cache local. Caso alguma linha seja alterada, ao executar novamente o comando de criação da imagem, apenas a linha alterada e as posteriores serão executadas novamente. 

Ao final do processo, executando `docker image ls` é possível ver a nova imagem na listagem de imagens existentes na estação de trabalho.

O nome da imagem é utilizado para criar um contêiner a partir dela:

```shel
docker container run -it --rm ubuntu:nginx
```

## Volumes

Da forma mais simples possível, volume é uma forma simples e performática de compartilhar dados entre o Docker host e o contêiner. O sistema de arquivos de um contêiner é composto de camadas em sua maioria somente-leitura, e realiza copia dos arquivos a serem alterados para uma camada de escrita, para manter sua integridade original, o que implica em perda de performance. Volumes também resolve questões de persistência de dados, pois informações armazenadas no contêiner são perdidas na sua exclusão. 

Ao criar um volume e definir seu mapeamento, toda a informação armazenada -  no diretório especificado para o contêiner - será armazenada não na camada de escrita do contêiner, mas no diretório mapeado do Docker host.

O mapeamento de volume segue o seguinte padrão:

```shell
-v "<host>:<container>" 
```

### Mapeamento de pasta específica do host

Faz o mapeamento entre uma pasta específica do host (ponto de montagem) para uma pasta do contêiner. 

No exemplo a seguir é criado um contêiner em que o diretório corrente é mapeado para o diretório `/tmp` do contêiner: 

```shell
docker container run -it -v $(pwd):/tmp alpine
```

Todo o conteúdo do diretório corrente torna-se disponível ao contêiner. Alterações na pasta `\tmp` do contêiner serão persistidas no host.

Esse tipo de mapeamento não é portável (pois não há garantias de que o ponto de montagem exista no Docker host. Por esse motivo, não pode ser feito via Dockerfile). 

> Alguns autores falam que diretórios precisam existir para que o contêiner funcione adequadamente. Nos testes realizados com a versão 19.03.11 do Docker, os diretórios inexistentes foram automaticamente criados em ambos os lados.

### Mapeamento de volumes nomeados

A partir da versão 1.9 o Docker permite a criação de volumes isolados de contêineres. Com isso temos portabilidade de dados, pois o volume torna-se independente do contêiner. 

Para visualizar os volumes criados no host:

```shell
docker volume ls
```

Para criar um volume:

```shell
docker volume create --name <nome>
```

Se não for usado o parâmetro `--name` o Docker dará um nome aleatório ao volume.

Para ver os detalhes de um volume:

```shell
docker volume inspect <nome> | <id>
```

Para remover um volume específico:

```shell
docker volume rm <nome>
```

Para remover todos os volumes não utilizados por contêineres:

```shell
docker volume prune
```

Volumes nomeados associados à contêineres não são excluídos estes são deletados. Logo, o `prune` pode ser usado para liberar espaço no host.

A associação entre volume e contêiner é parecida com a técnica de mapeamento de host, no entanto, ao invés de informar uma pasta no host, deve-se informar o nome do volume criado.

```shell
docker container run -v <nome-volume>:<container> <imagem>
```

Essa mesma sintaxe pode ser utilizada para criar um volume nomeado no momento da criação do contêiner, dispensando o uso do `docker volume create`. Para isso basta informar o nome desejado para o volume ao executar o `docker run`. 

É possível criar volumes somente-leitura, em que o contêiner não pode escrever dados, ou tornar um volume somente-leitura para um determinado contêiner. Para isso, basta adicionar `ro` ao parâmetro `-v`:

```shell
docker container run -v <nome-volume>:<container>:ro <imagem>
```

#### Exemplo prático

Criando um volume para armazenar os dados persistidos num banco de dados Postgres:

```shell
docker volume create --name pgdata
```

Associando o volume ao contêiner: 

```shell
docker container run -d -v pgdata:/var/lib/postgresql/data postgres
```

Usando volumes "isolados" é possível compartilhar o volume com diversos contêineres. A exclusão atualização de contêineres não implicará em perda de dados, pois o volume não será apagado.

> Imagens Docker podem definir a criação de volumes na criação de contêineres, através da seção `VOLUMES` do Dockerfile. Do exemplo acima, a imagem do Postgres força a criação do volume `/var/lib/postgresql/data` no qual armazenará os dados do banco. Ao associar o volume nomeado `pgdata` ao volume `/var/lib/postgresql/data` fazemos com que esses dados não sejam perdidos ao deletar o contêiner. Os volumes definidos na imagem Docker pode ser vistos com o comando `docker inspect <imagem>` na propriedade "Volumes" do JSON exibido.

## Redes

Redes em Docker são abstrações criadas (sobre o conceito de SDN - *Software Defined Networking*) para facilitar a comunicação entre contêineres e nós externos ao ambiente docker. As SDNs tem o mesmo comportamento de redes tradicionais e utilizam portas e IPs regulares, no entanto não necessitam de interfaces físicas de rede. Sob as SDNs o Docker oferece a infraestrutura para as redes: serviço de nomes e roteamento. 

### Redes padrões do Docker

Quando o docker é iniciado no host, automaticamente são criadas as  redes Bridge, None e Host.

#### Bridge

Ao iniciar um contêiner, se não for definida nenhuma rede, ele será adicionado à rede padrão Bridge. Cada contêiner vinculado à essa rede receberá, automaticamente, o próximo endereço  disponível na rede IP `172.17.0.0/16`.  

> A rede Bridge faz bridge com a interface virtual `docker0` no Docker host, por isso os contêineres recebem IPs na mesma faixa definida para docker0. Docker cria regras NAT do iptables no Docker host, o que permite, no fim das contas, que os contêineres conectados à docker0 tenham acesso ao mundo externo à rede. 

Esquema de comunicação entre a rede Bridge e docker0:

<img src="https://developer.ibm.com/recipes/wp-content/uploads/sites/41/2016/05/docker0.png" title="" alt="https://developer.ibm.com/recipes/wp-content/uploads/sites/41/2016/05/docker0.png" data-align="center">

Os contêineres associados à rede Bridge podem se comunicar entre si através do endereço de IP, e caso o Docker host tenha acesso à internet eles também terão. 

A rede Bridge permite aos contêineres expor suas portas, o que torna possível o acesso externo através do host.

#### None

Tem o objeto de isolar o contêiner de comunicações externas. É normalmente utilizada por contêineres que apenas manipulam arquivos, sem necessidade de enviá-los via rede para outro local. 

#### Host

Tem o objetivo de entregar para o contêiner todas as interfaces existentes no Docker host. Nesse tipo de rede é como se o contêiner utilizasse a interface física e portas do host. Não é possível levantar dois contêineres que exponham a mesma porta. 

### Redes definidas pelo usuário

O permite a criação de redes customizadas, devendo cada rede estar associada ao que o docker chama de driver de rede. A seguir são apresentados detalhes dos drivers Bridge e Overlay.

#### Bridge

É o driver de rede mais simples de utilizar, pois demanda pouca configuração. As redes criadas com esse driver são similares à rede padrão "bridge". 

Uma das diferenças entre a rede padrão bridge e as redes criadas com o driver bridge é contêineres associados a uma rede com baseada no drive bridge podem se comunicar entre si usando o nome ao invés do IP, isso porque essas redes usam o DNS interno do Docker, que associam os nomes dos contêineres desta rede com seus IPs e fornece de forma transparente o serviço de tradução. 

Um contêiner associado a uma determinada rede não pode acessar um contêiner de outra rede, mesmo se seu IP for conhecido. No entanto, podem expor suas portas no Docker host, e, por meio desse, serem acessados por contêineres externos à rede ou máquinas com acesso ao host.

<img src="https://stack.desenvolvedor.expert/appendix/docker/images/network_access.png" title="" alt="  " data-align="center">

#### Overlay

O driver overlay visa facilitar a comunicação entre contêineres docker que estão em redes distintas, mesmo que uma rede seja privada e a outra esteja numa infraestrutura pública na nuvem. O ponto essencial é: dados dois hosts rodando Docker, uma rede Overlay cria uma sub-rede que permite aos contêineres desses hosts - que estejam conectados a ela -  comunicarem em si usando IP, sub-rede e gateway padrão, como se fizessem parte da mesma rede.  

Redes overlay necessitam de configuração mais complexa. 

### Gerenciamento de redes

Para listar as redes no Docker host:

```shell
docker network ls
```

Para ver os detalhes de uma rede:

```shell
docker network inspect <nome-rede>
```

Para criar uma rede:

```shell
docker network create --driver <nome-driver> <nome-rede>
```

Se não for definido o parâmetro `--drive` será utilizado o padrão: bridge. Esse parâmetro pode ser substituído pelo seu apelido `-d`.

Os parâmetros `--subnet` e `--gateway` podem ser utilizados para configurar a LAN.

Para remover uma rede:

```shell
docker network rm <nome-rede>
```

Para remover todas redes não associadas a contêineres:

```shell
docker network prune
```

Para associar um contêiner a uma rede:

```shell
docker conteiner run --network <nome-rede> 
```

O parâmetro `--network` pode ser substituído pelo seu apelido `--net`.

## Docker Compose

É uma ferramenta para definição e execução de múltiplos contêineres . A partir de um arquivo de configuração, o  `docker-compose.yml`, é possível configurar todos os parâmetros para a execução de cada contêiner  da solução, dentre eles: rede, volumes, exposição de portas e variáveis de ambiente.  Os `services`  definidos no `docker-compose.yml` serão os contêineres criados ao levantar a aplicação. 



Com o YAML  a hierarquia é definida pela indentação.



Exemplo comentado 1: levantando um banco de dados Postgres e o cliente PgAdmin. 

```yml
version: '3'                                       # versao do docker-compose

services:                                          # define os serviços da solução
  server:                                          # serviço 1: 'server' 
    image: postgres                                # imagem base do contêiner
    environment:                                   # variáveis de ambiente p/ o ctnr
      POSTGRES_PASSWORD: "Postgres"
    ports:                                         # mapeamento de porta host:ctnr
      - "15432:5432"
    volumes:                                       
      - ./PostgreSQL:/var/lib/postgresql/data      # mapeamento de pasta específica
    networks:
      - pg-network                                 # vincula o ctnr à rede pg-network
      
  pgadmin:                                         # serviço 2: 'pgadmin'
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: "pgadmin@mail.com"
      PGADMIN_DEFAULT_PASSWORD: "PgAdmin"
    ports:
      - "16543:80"
    depends_on:                                     # 'pgadmin' depende 'server' p/ inciar 
      - server
    networks:
      - pg-network

networks:                                           # redes definidas pelo usuário
  pg-network:                                       # nome da rede
    driver: bridge

```



Exemplo comentado 2: usando Dockerfile.

```yml
version: '3'        # versao do docker-compose

services:           # define os serviços da solução
  web:              # serviço: 'web' 
    build: .        # usa o Dockerfile que está na mesma pasta que o docker-comopser.yml


```

Nesse caso o parâmetro `build` (equivalente ao comando `docker build` ) informa que o serviço `web` não será criado a partir de uma imagem pronta, ao contrário, a imagem para este serviço será construída antes de sua execução.



Exemplo comentado 3: definindo contexto e Dockerfile customizado. 

```yml
version: '3'                                # versao do docker-compose

services:                                   # define os serviços da solução
  web:                                      # serviço: 'web' 
    build:
      context: ./code                       # diretório contexto p/ criação da imagem
      dockerfile: Dockerfile-alternate      # Dcokerfile customizado 

```



Exemplo 4: levantando aplicação com Node Express.



Num diretório contendo os seguintes arquivos:

```json
package.json
{
	"name": "docker-node",
	"version": "1.0.0",
	"main": "index.js",
	"license": "MIT",
	"scripts": {
		"start": "node index.js"
	},
	"dependencies": {
		"express": "^4.17.1"
	}
}
```



```js
// index.js

var express = require('express');
var app = express();

app.get('/', (req, res) => {
	res.send(' <h1> Olá Mundo com Node Express e Docker </h1>');
});

app.listen(3000, () => {
	console.log('Aplicação ouvindo na porta 3000');
});
```



```dockerfile
FROM node:latest
RUN mkdir app
WORKDIR /app
COPY package.json /app 
RUN  npm install
COPY index.js /app
CMD npm run start
```



```yml
version: '3'

services:
  web:
    build: .
    ports:
      - "3000:3000"
```



### Gerenciando composições



Para inciar todos os serviços definidos no `docker-compose.yml`

```shell
docker-compose up
```

Pode-se utilizar os parâmetros `-d` para executar todos os serviços em background  e `--build` para gerar novamente a imagem a partir do Dockerfile.



Para listar todos os serviços:

```shell
docker-compose ps
```



Para parar todos os serviços:

```shell
docker-compose stop
```



Para parar todos os serviços e remover todos os contêineres e redes:

```
docker-compose down
```



## Referências

Esse documento é o resultado do estudo das fontes seguintes (públicas), e pode conter recortes e/ou imagens destas, visando unicamente criar um dicionário de conhecimento sobre o tema. 

- Rafael Gomes. **Docker para desenvolvedores**, 2020. Disponível em [Leanpub](http://leanpub.com/dockerparadesenvolvedores)

- **Documentação do docker**. Disponível em https://docs.docker.com/get-started/

- [Diferença entre imagem docker e contêiner](https://www.iperiusbackup.net/pt-br/qual-a-diferenca-entre-uma-imagem-docker-e-conteiner-como-criar-um-arquivo-docker-2)

- [Docker &mdash; contêineres](https://www.gta.ufrj.br/ensino/eel879/trabalhos_v1_2017_2/dockercontêineress.html)

- [Minicurso Docker - Apresentando Volumes - YouTube](https://youtu.be/LqiOuJYC2oM)

- [Utilizando volumes no Docker - YouTube](https://youtu.be/gU_3fW2VFPc)

- [Docker - Uma introdução básica - XII: Redes](http://www.macoratti.net/19/01/intro_docker12.htm)

- [Networking your docker containers using docker0 bridge](https://developer.ibm.com/recipes/tutorials/networking-your-docker-containers-using-docker0-bridge/)

- [Networking — Qual é a relação entre docker0 e eth0?](https://www.it-swarm.dev/pt/networking/qual-e-relacao-entre-docker0-e-eth0/826312383/)

- [Use bridge networks | Docker Documentation](https://docs.docker.com/network/bridge/)

- [Docker Networking Options - YouTube](https://www.youtube.com/watch?v=Yr6-2ddhLVo)

- [Networking with standalone containers | Docker Documentation](https://docs.docker.com/network/network-tutorial-standalone/)

- [VirtualBox - Entenda a Configuração da Placa de Rede na Máquina Virtual - Placa em Modo Bridge - YouTube](https://www.youtube.com/watch?v=_jsR6CFbVnI)

- [Entendendo a rede no Docker · Docker para Desenvolvedores](https://stack.desenvolvedor.expert/appendix/docker/rede.html)

- [Docker Overlay Driver and Overlay Networking](https://linuxhint.com/docker_overlay_driver/)

- [Docker e Docker Compose do zero ao Deploy - YouTube](https://www.youtube.com/watch?v=yb2udL9GG2U)

- [Docker e docker-compose na prática - YouTube](https://www.youtube.com/watch?v=YlYTnRRDRyM)

- [Docker Compose: O que é? Para que serve? O que come?](https://imasters.com.br/banco-de-dados/docker-compose-o-que-e-para-que-serve-o-que-come)

- [Compose file version 3 reference | Docker Documentation](https://docs.docker.com/compose/compose-file/)
